<?php 
    
    // Base class
    abstract class Supervisor 
    {     
        protected $slogan;    
         
        abstract function saySloganOutLoud();   
    }

    // interface
    interface Boss 
    {
        public function checkValidSlogan();
    }

    // trait
    trait Active 
    {
        /**
         * Echo class name
         *
         * @return string
         */
        public function defindYourSelf() {
            return get_class($this);
        }
    }
    
    // Children class EasyBoss
    class EasyBoss extends Supervisor implements Boss 
    {
        use Active;

        /**
         * Set value
         *
         * @param string $slogan
         */
        function setSlogan($slogan) {
            $this->slogan = $slogan;
        }

        /**
         * Check string
         *
         * @return boolean
         */
        public function checkValidSlogan() {
            if (strpos($this->slogan, 'before') !== false || strpos($this->slogan, 'after') !== false) {
                return true;
            }
            return false;
        }

        /**
         * echo
         *
         */
        public function saySloganOutLoud() {
            echo $this->slogan;
        }
    }
    
    // Children class UglyBoss
    class UglyBoss extends Supervisor implements Boss 
    {
        use Active;

        /**
         * Set value
         *
         * @param string $slogan
         */
        function setSlogan($slogan) {
            $this->slogan = $slogan;
        }

        /**
         * Check string
         *
         * @return boolean
         */
        public function checkValidSlogan() {
            if (strpos($this->slogan, 'before') !== false && strpos($this->slogan, 'after') !== false) {
                return true;
            }
            return false;
        }

        /**
         * echo
         *
         */
        public function saySloganOutLoud() {
            echo $this->slogan;
        }    
    }

    $easyBoss = new EasyBoss();
    $uglyBoss = new UglyBoss();

    $easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');
    $uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');
    
    $easyBoss->saySloganOutLoud(); 
    echo "<br>";
    $uglyBoss->saySloganOutLoud(); 

    echo "<br>";

    var_dump($easyBoss->checkValidSlogan());
    echo "<br>";
    var_dump($uglyBoss->checkValidSlogan());

    echo "<br>";

    echo 'I am ' . $easyBoss->defindYourSelf();
    echo "<br>";        
    echo 'I am ' . $uglyBoss->defindYourSelf(); 

?>
