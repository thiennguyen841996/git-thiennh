<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * @var User
     */
    protected $user;

    /**
     * UserController constructor.
     * 
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the users.
     *
     * @return view users.index
     */
    public function index(Request $request)
    {
        $users = $this->user->getUser($request);
        return view('users.index', ['users' => $users, 'request' => $request]);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return view users.create
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created user in users.
     *
     * @param  CreateUserRequest  $request
     * @return view users.index
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        $this->user->createUser($data);
        flash('Thêm mới người dùng thành công')->success();
        return redirect()->route('users.index');
    }
}
