<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloWorldController extends Controller
{
    /**
     * Display.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('hello_word.show');
    }
}
