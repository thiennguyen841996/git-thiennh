<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mail_address' => 'required|string|email|max:100|unique:users,mail_address',
            'password' => 'required|string|max:255',
            'password_confirmation' => 'required_with:password|same:password', 
            'name' => 'required|max:255',
            'address' => 'max:255',
            'phone' => 'numeric|digits_between:1,15',
        ];
    }
}
