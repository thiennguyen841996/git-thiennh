<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash;
use flash;

class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'mail_address', 
        'password', 
        'phone', 
        'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var int
     */
    protected $perPage = 20;

    /**
     * Create new user in table Users 
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }

    /**
     * Get all user in table Users 
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUser($request)
    {
        $builder = User::orderBy('mail_address', 'ASC');
        if (isset($request['email'])) {
            $builder->where('mail_address', 'like', '%' . $request->email . '%');
        }
        if (isset($request['address'])) {
            $builder->where('address', 'like', '%' . $request->address . '%');
        }
        if (isset($request['name'])) {
            $builder->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request['phone'] != null) {
            $builder->where('phone', '=', $request->phone);
        }
        return $builder->paginate();
    }
}
