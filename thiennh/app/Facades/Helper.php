<?php

namespace App\Facades;

class Helper
{
    /**
     * Upper case string.
     *
     * @param  String  $str
     * @return String
     */
    public function toUpperCase($str)
    {
        return mb_strtoupper($str, 'UTF-8');
    }
}
