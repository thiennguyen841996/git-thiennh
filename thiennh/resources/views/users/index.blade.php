@extends('layouts.default')

@section('head.title')
index
@endsection

@section('header.title')
Danh sách người dùng
@endsection

@section('content')
<div class="container">
    <a class="btn btn-info" href="{{ route('users.create') }}">Đăng kí</a>
    <div class="d-flex justify-content-center h-100">
        <div class="searchbar">
            <form class="form-inline" action="{{ route('users.index') }}" method="GET">
                <div class="form-group">
                    <label for="search">email:</label>
                    <input type="text" class="form-control" name="email" value="{{ $request->email }}">
                    <label for="search">name:</label>
                    <input type="text" class="form-control" name="name" value="{{ $request->name }}">
                    <label for="search">address:</label>
                    <input type="text" class="form-control" name="address" value="{{ $request->address }}">
                    <label for="search">phone:</label>
                    <input type="text" class="form-control" name="phone" value="{{ $request->phone }}">
                    <button class="btn btn-info" type="submit">Search</button>
                </div>
            </form>
        </div>
    </div>
    @include('flash::message')
    <div class="table-responsive">
    <table class="table">
    <thead>
        <tr>
        <th class="text-center">Số thứ tự</th>
        <th class="text-center">Địa chỉ email</th>
        <th class="text-center">Tên</th>
        <th class="text-center">Địa chỉ</th>
        <th class="text-center">Số điện thoại</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
        <td class="text-left">{{ (($users->currentPage() - 1) * $users->perPage()) + $loop->iteration }}</td>
        <td class="text-left">{{ $user->mail_address }}</td>
        <td class="text-left">{{ Helper::toUpperCase($user->name) }}</td>
        <td class="text-left">{{ $user->address }}</td>
        <td class="text-left">{{ $user->phone }}</td>
        </tr>
        @endforeach
    </tbody>
    </table>
    <div class="text-right">
        {{ $users->links() }}
    </div>
</div>
@endsection
