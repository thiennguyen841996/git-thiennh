@extends('layouts.default')

@section('head.title')
create
@endsection

@section('header.title')
Thêm mới người dùng
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <form action="{{ route('users.store')}}" method="post" id="fileForm" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <fieldset>
                <div class="form-group row {{ $errors->has('mail_address') ? 'text-danger' : '' }}">
                    <label for="email" class="col-sm-4 col-form-label"><span class="req">* </span> Địa chỉ email: </label>
                    <div class="col-sm-8">
                        <input class="form-control {{ $errors->has('mail_address') ? 'border border-danger' : '' }}" type="text" name="mail_address" />
                        @if ($errors->has('mail_address'))
                            {{ $errors->first('mail_address') }}
                        @endif
                    </div>
                </div>
                <div class="form-group row {{ $errors->has('password') ? 'text-danger' : '' }}">
                    <label for="password" class="col-sm-4 col-form-label"><span class="req">* </span> Mật khẩu: </label>
                    <div class="col-sm-8">
                        <input class="form-control {{ $errors->has('password') ? 'border border-danger' : '' }}" type="password" name="password" />
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                        @endif
                    </div>
                </div>
                <div class="form-group row {{ $errors->has('password_confirmation') ? 'text-danger' : '' }}">
                    <label for="password" class="col-sm-4 col-form-label"><span class="req">* </span> Mật khẩu xác nhận: </label>
                    <div class="col-sm-8">
                        <input class="form-control {{ $errors->has('password_confirmation') ? 'border border-danger' : '' }}" type="password" name="password_confirmation" />
                        @if ($errors->has('password_confirmation'))
                            {{ $errors->first('password_confirmation') }}
                        @endif
                    </div>
                </div>
                <div class="form-group row {{ $errors->has('name') ? 'text-danger' : '' }}">
                    <label for="name" class="col-sm-4 col-form-label"><span class="req">* </span> Tên: </label>
                    <div class="col-sm-8">
                        <input class="form-control {{ $errors->has('name') ? 'border border-danger' : '' }}" type="text" name="name" />
                        @if ($errors->has('name'))
                            {{ $errors->first('name') }}
                        @endif
                    </div>
                </div>
                <div class="form-group row {{ $errors->has('address') ? 'text-danger' : '' }}">
                    <label for="address" class="col-sm-4 col-form-label"><span class="req">* </span> Địa chỉ: </label>
                    <div class="col-sm-8">
                        <input class="form-control {{ $errors->has('address') ? 'border border-danger' : '' }}" type="text" name="address" />
                        @if ($errors->has('address'))
                            {{ $errors->first('address') }}
                        @endif
                    </div>
                </div>
                <div class="form-group row {{ $errors->has('phone') ? 'text-danger' : '' }}">
                    <label for="address" class="col-sm-4 col-form-label"><span class="req">* </span> Số điện thoại: </label>
                    <div class="col-sm-8">
                        <input class="form-control {{ $errors->has('phone') ? 'border border-danger' : '' }}" type="text" name="phone" />
                        @if ($errors->has('phone'))
                            {{ $errors->first('phone') }}
                        @endif
                    </div>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-success" type="submit">Đăng kí</button>
                </div>
            </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection
