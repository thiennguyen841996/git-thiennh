<?php
    session_start();
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang home</title>
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container" style="margin-top: 20px;">
    <?php 
    if(isset($_SESSION["email"])) {
        // Hiển thị thông báo đăng nhập thành công 1 lần
        if (isset($_SESSION["login_success"])) {
            echo $_SESSION["login_success"];
            echo '<br>';
        }
        unset($_SESSION['login_success']);
        ?>
        Xin chào 
        <?php echo $_SESSION["email"]; ?>.
        <form class="form-horizontal" method="POST" action="LoginSuccessPdo.php">
            <input class="btn btn-primary" type="submit" name="logout_action" value="Logout"/>
        </form>
        <?php 
    } else {
        echo '<h1>Bạn chưa đăng nhập</h1>';
        echo '<a href="./LoginPdo.php">Quay lại trang đăng nhập</a>';
    }

    if (!empty($_POST['logout_action'])) {
        session_destroy();
        if(isset($_COOKIE['email'])) {
            setcookie('email', '', time() - 360);
        }
        if(isset($_COOKIE['password'])) {
            setcookie('password', '', time() - 360);
        }
        header('Location: LoginPdo.php');
    }
    ?>
</div>
</body>
</html>
