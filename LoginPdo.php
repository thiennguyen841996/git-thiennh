<?php
    session_start();

    /**
     * check valid email
     *
     * @param string email
     * @return boolean
     */
    function validEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * check length of email
     *
     * @param string email
     * @return boolean
     */
    function checkLengthEmail($email)
    {
        return (strlen($email) < 256);
    } 

    /**
     * check length of password
     *
     * @param string password
     * @return boolean
     */
    function checkLengthPassword($password)
    {
        return strlen($password) >= 6 && strlen($password) <= 50;
    } 

    $errors = array();
    $data = array();

    if (isset($_POST['login_form'])) {
        // get data from method post
        $data['email'] = $_POST['email'] ?? '';
        $data['password'] = $_POST['password'] ?? '';

        // validate email
        if (empty($data['email'])) {
            $errors['email'] = 'Bạn chưa nhập email';
        } elseif (!validEmail($data['email']) && !checkLengthEmail($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng và độ dài vượt quá 255 ký tự'; 
        } elseif (!validEmail($data['email']) && checkLengthEmail($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng';
        } elseif (validEmail($data['email']) && !checkLengthEmail($data['email'])) {
            $errors['email'] = 'Email vượt quá 255 ký tự';
        }

        // validate password
        if (empty($data['password'])) {
            $errors['password'] = 'Bạn chưa nhập password';
        } elseif (!checkLengthPassword($data['password'])) {
            $errors['password'] = 'Password chỉ cho phép độ dài từ 6-50 ký tự';   
        }

        // work with database
        if (empty($errors)) {
            try {
                $conn = new PDO(
                    'mysql:host=localhost; dbname=thiennh; charset=utf8',
                    'root',
                    ''
                );
                $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS);
                $stmt = $conn->prepare("SELECT mail_address,password FROM users WHERE mail_address=:email && password=:password");
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':password', $password);
                $email = $data['email'];
                $password = $data['password'];
                $stmt->setFetchMode(PDO::FETCH_CLASS);
                $stmt->execute();
                $result = $stmt->fetchAll();      
                if (!$result) {
                    echo '<div class="text-danger text-center">Đăng nhập thất bại</div> <br>';
                } else {
                    $_SESSION['email'] = $_POST['email'];
                    $_SESSION['login_success'] = 'Đăng nhập thành công';
                    header('Location: LoginSuccessPdo.php');
                    // set cookie
                    if(!empty($_POST['remember_me'])) {
                        setcookie('email', $_POST['email'], time() + 3600);
                        setcookie('password', $_POST['password'], time() + 3600);
                    }
                }
            } catch (PDOException $ex) {
                echo 'Kết nối đến database không thành công';
            }
        }
    }
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Trang đăng nhập</title>
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="text-success text-center">
        <?php echo $_SESSION["register_success"] ?? ''; echo '<br>'; unset($_SESSION['register_success']); ?>
    </div>
    <h2>Đăng nhập</h2>
    <form method="POST" action="LoginPdo.php">
        <div class="form-group">
            <label class="control-label col-md-2">Email</label>
            <div class="col-md-10">
                <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $_COOKIE['email'] ?? '' ?>">
                <div class="text-danger"><?php echo $errors['email'] ?? ''; ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Mật khẩu</label>
            <div class="col-md-10">
                <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo $_COOKIE['password'] ?? '' ?>">
                <div class="text-danger"><?php echo $errors['password'] ?? ''; ?></div>
            </div>   
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">    
                <div class="checkbox">
                    <input type="checkbox" name="remember_me" <?php echo isset($_COOKIE['email']) ? 'checked' : '' ?>>Remember me
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">    
                <input type="submit" name="login_form" class="btn btn-primary" value="Đăng nhập ">
                <a href="RegisterPdo.php">Đăng ký</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>
