<?php

/**
 * Check string.
 *
 * @param string $str
 * @return boolean
 */
function checkValidString($str)
{
    if (strlen($str) == 0 || (strlen($str) > 50 && strpos($str, 'after') === false)  || (strpos($str, 'before') !== false && strpos($str, 'after') === false)) {
        return true;
    }
    return false;
}

/**
 * Read file.
 *
 * @param string $file
 * @return string
 */
function readString($file)
{
    $data = @fopen($file, 'r');
    $len = filesize($file);

    if ($len == 0) {
        $str = '';   
    } else {
        $str = fread($data, filesize($file));
    }
    fclose($data);

    return $str;
}

$str1 = readString('file1.txt');

if (checkValidString($str1)) {
    echo 'Chuoi hop le';
} else {
    echo 'Chuoi khong hop le';
}

echo '<br>';

$str2 = readString('file2.txt');

if (checkValidString($str2)) {
    echo 'Chuoi hop le';
} else {
    echo 'Chuoi khong hop le';
}

?>
