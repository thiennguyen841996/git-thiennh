<?php

class HandleString 
{
    // property
    public $check1;
    public $check2;
        
    /**
     * Read file.
     *
     * @param string $file
     * @return string
     */
    public function readFile($file)
    {
        $data = @fopen($file, 'r');
        $len = filesize($file);

        if ($len == 0) {
            $str = '';   
        } else {
            $str = fread($data, filesize($file));
        }
        fclose($data);

        return $str;
    }
   
    /**
     * Check string.
     *
     * @param string $str
     * @return boolean
     */
    public function checkValidString($str)
    {
        if (strlen($str) == 0 || (strlen($str) > 50 && strpos($str, 'after') === false)  || (strpos($str, 'before') !== false && strpos($str, 'after') === false)) {
            return true;
        }
        return false;
    }
}

// create new object1
$object1 = new HandleString();
$check1 = $object1->checkValidString($object1->readFile("file1.txt"));
$check2 = $object1->checkValidString($object1->readFile("file2.txt"));
$object1->check1 = $check1;
$object1->check2 = $check2;

?>
