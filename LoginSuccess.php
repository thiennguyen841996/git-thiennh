<?php
    session_start();
?>
<html>
    <head>
        <title>User Login</title>   
    </head>
    <body>
        <?php 
        if(isset($_SESSION["email"])) {
            // Hiển thị thông báo đăng nhập thành công 1 lần
            if (isset($_SESSION["success"])) {
                echo $_SESSION["success"];
                echo '<br>';
            }
            unset($_SESSION['success']);
            ?>
            Xin chào 
            <?php echo $_SESSION["email"]; ?>.
            <form method="POST" action="LoginSuccess.php">
                <input type="submit" name="logout_action" value="Logout"/>
            </form>
            <?php 
        } else {
            echo '<h1>Bạn chưa đăng nhập</h1>';
            echo '<a href="./Login.php">Quay lại trang đăng nhập</a>';
        }

        if (!empty($_POST['logout_action'])) {
            session_destroy();
            if(isset($_COOKIE['email'])) {
                setcookie('email', '', time() - 360);
            }
            if(isset($_COOKIE['password'])) {
                setcookie('password', '', time() - 360);
            }
            header('Location: Login.php');
        }
        ?>
    </body>
</html>
