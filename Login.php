<!DOCTYPE html>
<html>
    <head>
        <title>Login Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php
        session_start();
        // Code PHP xử lý validate
        $error = array();
        $data = array();
        if (!empty($_POST['login_action'])) {
            // Lấy dữ liệu
            $data['email'] = $_POST['email'] ?? '';
            $data['password'] = $_POST['password'] ?? '';

            // Kiểm tra định dạng dữ liệu
            /**
             * Check email
             *
             * @param string $email
             * @return boolean
             */
            function isEmail($email)
            {
                return (!filter_var($email, FILTER_VALIDATE_EMAIL)) ? false : true;
            }

            /**
             * check length email
             *
             * @param string $email
             * @return boolean
             */
            function lengthEmail($email)
            {
                return (strlen($email) < 256);
            } 

            /**
             * Check length password
             *
             * @param string $password
             * @return boolean
             */
            function lengthPassword($password)
            {
                return ((strlen($password) < 6) || (strlen($password) > 30));
            }

            // Kiểm tra định dạng email
            if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!isEmail($data['email'])) {
                $error['email'] = 'Email không đúng định dạng';
            } elseif (!lengthEmail($data['email'])) {
                $error['email'] = 'Email tối đa 255 kí tự';
            }

            // Kiểm tra định dạng password
            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập password';
            } elseif (lengthPassword($data['password'])) {
                $error['password'] = 'Password có độ dài từ 6 đến 30 kí tự';
            }

            // Xử lý đăng nhập
            /**
             * Check login
             *
             * @param string $email
             * @param string $password
             * @return boolean
             */
            function login($email, $password) 
            {
                if ($email == 'thiennh@gmail.com' && $password == 'thiennh123') {
                    return true;
                }
                return false;
            }

            if (!$error && login($_POST['email'], $_POST['password'])) {
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['success'] = 'Đăng nhập thành công';
                // chuyen sang trang LoginSuccess.php
                header('Location: LoginSuccess.php');
                // Xử lí remember
                if(!empty($_POST['remember'])) {
                    setcookie('email', $_POST['email'], time() + 3600);
                    setcookie('password', $_POST['password'], time() + 3600);
                }
            } else {
                echo 'Đăng nhập không thành công';
            }
        }
        ?>

        <h1>Login Form</h1>
        <form method="POST" action="Login.php">
            <table cellspacing="0" cellpadding="5">
                <tr>
                    <td>Email của bạn</td>
                    <td>
                        <input type="mail" name="email" id="email" value="<?php echo $_COOKIE['email'] ?? '' ?>"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p style="color: red"><?php echo $error['email'] ?? ''; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" id="password" name="password" value="<?php echo $_COOKIE['password'] ?? '' ?>">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p style="color: red"><?php echo $error['password'] ?? ''; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>Remember me</td>
                    <td><input type="checkbox" name="remember" id="remember" <?php echo isset($_COOKIE['email']) ? 'checked' : '' ?>></td>
                </tr>
                <tr>
                    <td><input type="reset" name="reset_action" value="Reset"/></td>
                    <td><input type="submit" name="login_action" value="Login"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>
