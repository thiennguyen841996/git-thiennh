<?php
    session_start();

    /**
     * check valid email
     *
     * @param string email
     * @return boolean
     */
    function validEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * check length of email
     *
     * @param string email
     * @return boolean
     */
    function checkLengthEmail($email)
    {
        return (strlen($email) < 256);
    }

    /**
     * check length of password
     *
     * @param string password
     * @return boolean
     */
    function checkLengthPassword($password)
    {
        return strlen($password) >= 6 && strlen($password) <= 50;
    } 

    $errors = array();
    $data = array();

    // if click button Login
    if (isset($_POST['signup_form'])) {
        //get data
        $data['email'] = $_POST['email'] ?? '';
        $data['password'] = $_POST['password'] ?? '';
        $data['password_confirm'] = $_POST['password_confirm'] ?? '';

        // validate email
        if (empty($data['email'])) {
            $errors['email'] = 'Bạn chưa nhập email';
        } elseif (!validEmail($data['email']) && !checkLengthEmail($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng và độ dài vượt quá 255 ký tự'; 
        } elseif (!validEmail($data['email']) && checkLengthEmail($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng';
        } elseif (validEmail($data['email']) && !checkLengthEmail($data['email'])) {
            $errors['email'] = 'Email vượt quá 255 ký tự';
        }

        // validate password
        if (empty($data['password'])) {
            $errors['password'] = 'Bạn chưa nhập password';
        } elseif (!checkLengthPassword($data['password'])) {
            $errors['password'] = 'Password chỉ cho phép độ dài từ 6-50 ký tự';   
        }  
        
        // validate password confirm
        if (empty($data['password_confirm'])) {
            $errors['password_confirm'] = 'Bạn chưa nhập password confirm';
        } elseif ($data['password_confirm'] !== $data['password']) {
            $errors['password_confirm'] = 'Password confirm không trùng khớp';   
        }

        // work with database
        if (empty($errors)) {
            try {
                $conn = new PDO(
                    'mysql:host=localhost; dbname=thiennh; charset=utf8',
                    'root',
                    ''
                );
                // prepare sql
                $stmt = $conn->prepare("INSERT INTO users (mail_address, password)
                VALUES (:email, :password)");
                // bind parameters
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':password', $password);
                $email = $data['email'];
                $password = $data['password'];
                $stmt->execute();
                $_SESSION['register_success'] = 'Đăng ký thành công';
                header('Location: LoginPdo.php');
            } catch (PDOException $ex) {
                echo 'Kết nối đến database không thành công';
            }
        } else {
            echo '<div class="text-danger text-center">Đăng ký thất bại</div> <br>';
        }
    }
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Trang đăng ký</title>
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h2 style="margin-top: 20px;">Đăng ký</h2>
    <form method="POST" action="RegisterPdo.php">
        <div class="form-group">
            <label class="control-label col-md-2">Email</label>
            <div class="col-md-10">
                <input type="email" class="form-control" name="email" placeholder="Email">
                <div class="text-danger"><?php echo $errors['email'] ?? ''; ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Mật khẩu</label>
            <div class="col-md-10">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <div class="text-danger"><?php echo $errors['password'] ?? ''; ?></div>
            </div>   
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Nhập lại mật khẩu</label>
            <div class="col-md-10">
                <input type="password" class="form-control" name="password_confirm" placeholder="Password confirm">
                <div class="text-danger"><?php echo $errors['password_confirm'] ?? ''; ?></div>
            </div>   
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">    
                <button type="submit" name="signup_form" class="btn btn-primary">Đăng ký</button>
                <a href="LoginPdo.php">Đăng nhập</a>
            </div>
        </div>    
    </form>
</div>
</body>
</html>
